# STIX2 jupyter-notebooks

@Nounou_Mbeiri Jupyter Notebooks

This jupyter notebook is a very useful example of how the jupyter tool can be used to manipulate a json files.
In this case, it is used to build a STIX2 Visualizer that allows us to visualize in a very fast way the TTPs embedded in a STIX2 file.

# Install requirements:
-pip install requirements.txt
